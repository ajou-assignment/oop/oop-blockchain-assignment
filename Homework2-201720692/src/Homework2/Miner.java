package Homework2;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Miner extends User {
    private Wallet miner;
    private final float REWARD_COIN = 12.5f;
    private final float REWARD_FEE = 0.0f;

    public static final int MINE_TRANSACTION_SIZE = 5;

    public Miner(Ledger ledger, ArrayList<Wallet> wallet, String minerName) {
        super(ledger, wallet);
        for (int i = 0; i < wallet.size(); i++) {
            if (wallet.get(i).getUserName().equals(minerName)) {
                miner = wallet.get(i);
            }
        }
    }

    //채굴한다. 명령어에 따라 미숭인트런적션의 허용크기플 다르개
    public void mine(int unconfirmedTransactionSizeLimit) {
        //list copy
        List<Transaction> unconfirmedTransactions = new ArrayList<Transaction>(publicLedger.getUnconfirmedTransaction());

        if(unconfirmedTransactions.size() >= unconfirmedTransactionSizeLimit) {

            ArrayList<Transaction> transactions = new ArrayList<Transaction>(unconfirmedTransactions);
            transactions.sort(new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    if(o1.getFee() > o2.getFee()) {
                        return -1;
                    }
                    else if(o1.getFee() < o2.getFee()) {
                        return 1;
                    }
                    return 0;
                }
            });

            transactions = new ArrayList<Transaction>(transactions.subList(0, Block.transactionNum - 1)); //for fake

            Block preBlock = publicLedger.getRecentBlock();
            Block block = createBlock(preBlock, transactions, false);
            System.out.println("블록 채굴 성공!");
            publicLedger.setNewBlock(block);
            System.out.println("New Block Is Validated!");
        }
    }

    private Block createBlock(Block preBlock, ArrayList<Transaction> transactions, boolean fake) {
        List<Block> blockchain = publicLedger.getBlockchain();
        int blockHeight = blockchain.size();
        String preBlockId;
        if(preBlock != null) {
            preBlockId = preBlock.getBlockID();
        }
        else {
            preBlockId = "";
        }

        Transaction rewardTransaction = new Transaction(miner, REWARD_COIN, REWARD_FEE );
        transactions.add(rewardTransaction);

        String[] leafs = transactions.stream().map(x -> x.getTxID()).toArray(String[]::new);
        MerkleTree merkleTree = new MerkleTree(leafs, "");
        String merkleRoot = merkleTree.getRoot();

        Pair<String, String> pair;
        if(fake == false) {
            pair = resolveHashcash(preBlockId, merkleRoot);
        }
        else {
            pair = resolveHashcash(preBlockId, "");
        }
        String blockId = pair.getKey();
        String blockMaker = miner.getUserName();
        String timestamp = Utils.getTimestamp();
        String nonce = pair.getValue();

        Block block = new Block(blockId, blockMaker, blockHeight,
                timestamp, nonce, preBlockId,
                merkleRoot, merkleTree,
                transactions);
        return block;
    }

    private Pair<String, String> resolveHashcash(String preBlockId, String merkleRoot) {
        String hash, nonce;
        do {
             nonce = Integer.toString(Utils.Rand.Int());
             hash = Block.getValidHash(preBlockId, merkleRoot, nonce);
        } while(hash.isEmpty());
        Pair pair = new Pair<String, String>(hash, nonce);
        return pair;
    }

    //가짜 블록을 만든다. 오직 Nonce만으로 앞자리가 0며 3개가 되는 해시값을 만든다. 었는 Random하게 션택호10한다.
    public void makeFakeBlock() {
        ArrayList<Transaction> unconfirmedTransactions = new ArrayList<Transaction>();
        for(int i = 0; i < Block.transactionNum; i++) {
            Transaction transaction = createRandomTransaction(false);
            unconfirmedTransactions.add(transaction);
        }
        Block fakeBlock = createBlock(null, unconfirmedTransactions, true);
        publicLedger.setNewBlock(fakeBlock);
    }
}