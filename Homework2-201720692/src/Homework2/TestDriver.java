package Homework2;

import javax.xml.bind.ValidationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TestDriver {


	public static int checkCommand(ArrayList<CommandScheme> commandSchemes, String[] cmds) {
		int commandIdx = -1;
		String action = cmds[0];
		CommandScheme commandScheme = null;
		int i;
		for(i = 0; i < commandSchemes.size(); i++) {
			commandScheme = commandSchemes.get(i);
			if(action.equals(commandScheme.getCommandName())) {
				commandIdx = commandSchemes.indexOf(commandScheme);
				break;
			}
		}
		if(i == commandSchemes.size()) {
			throw new IllegalArgumentException("없는 명령어입니다");
		}


		if(cmds.length - 1 != commandScheme.getParameterNum()) {
			throw new IllegalArgumentException("매개변수의 갯수가 맞지않습니다.");
		}

		String[] parameterTypes = commandScheme.getParameterTypes();
		for(i = 1; i < commandScheme.getParameterNum() + 1; i++) {
			String parameterType = parameterTypes[i-1];
            String parameter = cmds[i];
		    if(parameterType.equals(CommandScheme.Types.INTEGER)) {
                Utils.isInt(parameter);
            }
            else if(parameterType.equals(CommandScheme.Types.FLOAT)) {
		        Utils.isFloat(parameter);
            }
		}


		return commandIdx;
	}


    public static void send(String[] args, Ledger ledger, ArrayList<Wallet> wallets, User user, Miner miner) throws ValidationException {
	    String sender = args[1];
	    String receiver = args[2];
	    float coin = Float.parseFloat(args[3]);
	    float fee = Float.parseFloat(args[4]);
        if(coin < 0 || fee < 0) {
            throw new IllegalArgumentException("보내는 코인 혹은 수수료가 음수 입니다.");
        }

        Wallet senderWallet = user.findWallet(sender);
	    Wallet receiverWallet = user.findWallet(receiver);

        if(coin + fee > senderWallet.getUserCoin()) {
            throw new IllegalArgumentException("Sender가 충분한 코인을 가지고 있지 않습니다.");
        }
	    user.sendTransaction(senderWallet, receiverWallet, coin, fee);
	    miner.mine(Miner.MINE_TRANSACTION_SIZE);
	    user.checkNewBlock();
    }

    public static void createTransaction(String[] args, Ledger ledger, ArrayList<Wallet> wallets, User user, Miner miner)  throws ValidationException{
	    int transactionNum = Integer.parseInt(args[1]);
	    if(transactionNum > 100) {
            throw new IllegalArgumentException("너무 많은 트랙잭션 요청");
        }
        for(int i = 0; i < transactionNum; i++) {
	        user.createRandomTransaction(true);
	        miner.mine(Miner.MINE_TRANSACTION_SIZE);
	        user.checkNewBlock();
        }
    }

    public static void block(String[] args, Ledger ledger, ArrayList<Wallet> wallets, User user, Miner miner) {
        int blockHeight = Integer.parseInt(args[1]);
        Block block = ledger.findBlock(blockHeight);
        System.out.println("Block Found!");
        System.out.println(block);
    }


    public static void wallet(String[] args, Ledger ledger, ArrayList<Wallet> wallets, User user, Miner miner) {
        Wallet wallet = user.findWallet(args[1]);
        System.out.println(wallet);
    }

    public static void createFakeBlock(String[] args, Ledger ledger, ArrayList<Wallet> wallets, User user, Miner miner) throws ValidationException {
        System.out.println("Attack the blockchain!");
	    miner.makeFakeBlock();
        user.checkNewBlock();
    }


	public static void status(String[] args, Ledger ledger, ArrayList<Wallet> wallets, User user, Miner miner) {
        System.out.println(wallets);
        System.out.println(String.format("Current Height : %d",ledger.getBlockchain().size()));
        Block block = ledger.getRecentBlock();
        if(block != null) {
            System.out.println("=========================Up to Date Block===============\n");
            System.out.println(block);
        }
        List<Transaction> unconfirmedTransaction = ledger.getUnconfirmedTransaction();
		System.out.println("=========================Unconfirmed Transaction===============");
		System.out.println(unconfirmedTransaction);
    }

    public static void flush(String[] args, Ledger ledger, ArrayList<Wallet> wallets, User user, Miner miner) throws ValidationException {
        miner.mine(3);
		user.checkNewBlock();
    }


    public static void main(String[] args) {
		Scanner cli = new Scanner(System.in);

		//이 리스트를 통해 List<Wallet>을 초기화 할것입니다.
		final int initCoin = 100;

		String minerName = "miner";
		String[] userNames = {"a",
				"b",
				"c",
				"d",
				minerName}; //initminer는 채굴자의 이름.


		//Make Command Scheme, 이걸 기준으로 Exception을 생성하면 편할것입니다.
		String[] commands = {"send", "create_transaction", "block", "wallet", "create_fakeblock", "status", "flush"};
		int[] eachCommandsParameterNum = {4, 1, 1, 1, 0, 0, 0};
		String[][] eachCommandParameterTypes ={{CommandScheme.Types.STRING,CommandScheme.Types.STRING, CommandScheme.Types.FLOAT, CommandScheme.Types.FLOAT},
												{CommandScheme.Types.INTEGER},
												{CommandScheme.Types.INTEGER},
												{CommandScheme.Types.STRING},
												{},
												{},
												{}};

		ArrayList<CommandScheme> commandScheme = new ArrayList<CommandScheme>();
		for(int i=0; i< commands.length; i++) {
			commandScheme.add(new CommandScheme(commands[i], eachCommandsParameterNum[i], eachCommandParameterTypes[i]));
		}

		User dijkstra; //User에는 Wallet과 Ledger가 들어갈것
		Miner euler;  //Miner에도 Wallet과 Ledger가 들어갈것.

		Ledger ledger; //User Node와 Miner Node가 공유할 장부
		ArrayList<Wallet> wallets = new ArrayList<Wallet>();//User Node와 Miner Node가 공유할 지갑

		//Ledger를 초기화
		ledger = new Ledger();

		//Wallet List를 초기화
		for(String userName : userNames) {
			Wallet wallet = new Wallet(userName, initCoin);
            wallets.add(wallet);
		}

		//Ledger와 Wallet 리스트를 초기화하고, 이것을 통해 user node와 miner node를 초기화 하는 코드가 여기 들어간다.
		dijkstra = new User(ledger, wallets);
		euler = new Miner(ledger, wallets, minerName);

		String cmd = "";
		String[] cmdSplited;
		int commandIdx = -1;
		while(true){
			try {
				System.out.print(">");
				System.out.flush();
				cmd = cli.nextLine();
				cmdSplited = cmd.split(" ");

				commandIdx = checkCommand(commandScheme, cmdSplited);

				//커맨드의 유효성을 확인한다.
				if(commandIdx == 0) {
					//send 명령어의 검증및 실행
                    send(cmdSplited, ledger, wallets, dijkstra, euler);
				}
				else if(commandIdx == 1) {
					//create_transaction 명령어의 검증 및 실행
                    createTransaction(cmdSplited, ledger, wallets, dijkstra, euler);
				}
				else if(commandIdx == 2) {
					//block 명령어의 검증 및 실행
                    block(cmdSplited, ledger, wallets, dijkstra, euler);
				}
				else if(commandIdx == 3) {
					//wallet 명령어의 검증 및 실행
                   wallet(cmdSplited, ledger, wallets, dijkstra, euler);
				}
				else if(commandIdx == 4) {
					//create_fakeblock 명령어의 검증 및 실행
                    createFakeBlock(cmdSplited, ledger, wallets, dijkstra, euler);
				}
				else if(commandIdx == 5) {
					//status 명령어의 실행
                    status(cmdSplited, ledger, wallets, dijkstra, euler);
				}
				else if(commandIdx == 6) {
					//flush 명령어의 실행
                    flush(cmdSplited, ledger, wallets, dijkstra, euler);
				}

			}
			catch(Exception e) {
				e.printStackTrace();
			}




		}

	}
}
