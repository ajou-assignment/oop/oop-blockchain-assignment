package Homework2;

import java.util.List;
import java.util.ArrayList;

public class Ledger {
    private List<Block> blockchain;
    private List<Transaction> unconfirmedTransaction;
    private Block newBlock;
    private boolean isNewBlockCreated;

    public Ledger() {
        blockchain = new ArrayList<Block>();
        unconfirmedTransaction = new ArrayList<Transaction>();
    }

    public Block findBlock(int blockHeight) {
        Block block;
        try {
            block = blockchain.get(blockHeight);
            return block;
        }
        catch(Exception e) {
            throw new IllegalArgumentException("존재하지 않는 블록입니다.");
        }
    }

    public void addBlock(Block block) {
        //TODO
        blockchain.add(block);
    }

    public void setNewBlock(Block newBlock) {
        this.newBlock = newBlock;
        this.isNewBlockCreated = true;
    }

    public void cleanNewBlock() {
        this.newBlock = null;
        this.isNewBlockCreated = false;
    }

    public Block getRecentBlock() {
        int currentBlockHeight = blockchain.size();
        int last = currentBlockHeight - 1;
        if(last < 0)
            return null;
        return blockchain.get(last);
    }

    public void addUnconfirmedTransaction(Transaction transaction) {
        unconfirmedTransaction.add(transaction);
    }

    public List<Block> getBlockchain() {
        return blockchain;
    }

    public void setBlockchain(List<Block> blockchain) {
        this.blockchain = blockchain;
    }

    public List<Transaction> getUnconfirmedTransaction() {
        return unconfirmedTransaction;
    }

    public void setUnconfirmedTransaction(List<Transaction> unconfirmedTransaction) {
        this.unconfirmedTransaction = unconfirmedTransaction;
    }

    public Block getNewBlock() {
        return newBlock;
    }


    public boolean isNewBlockCreated() {
        return isNewBlockCreated;
    }

    public void setNewBlockCreated(boolean newBlockCreated) {
        isNewBlockCreated = newBlockCreated;
    }

}
