package Homework2;


public class CommandScheme {
    private String commandName;
    private int parameterNum;
    private String[] parameterTypes;

    public class Types {
        public static final String STRING = "String";
        public static final String INTEGER = "Integer";
        public static final String FLOAT = "Float";
    }

    public CommandScheme(String name, int num, String[] parameterTypes) {
        this.commandName = name;
        this.parameterNum = num;
        this.parameterTypes = parameterTypes;
    }

    public String[] getParameterTypes() {
        return parameterTypes;
    }

    public String getCommandName() {
        return commandName;
    }

    public int getParameterNum() {
        return parameterNum;
    }
}