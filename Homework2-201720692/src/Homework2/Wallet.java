package Homework2;

public class Wallet {
    private String userName;
    private float userCoin;

    public Wallet() {

    }


    public Wallet(String userName, float initCoin) {
        this.userName = userName;
        this.userCoin = initCoin;
    }

    //create_transaction 명령어量 위해 현재의 유저한도 내에서 랜덤한 크기의 숭금용 코인 크기 설정
    public float getRandomCoinToSend() {
        if(userCoin < 3.0f)
            return 0.0f;

        float point = 1 * 10;
        float min = 1 / 1.1f * point;
        float max = userCoin / 1.1f * point;
        float randomCoin = Utils.Rand.Float() * (max - min + 1.0f) + min;
        randomCoin = (int)randomCoin / point ;
        return randomCoin;
    }

    //트련잭션 반영시 유저 보유 코인 증가
    public void increaseCoin(float coin) {
        userCoin += coin;
    }

    //트런잭션量 반영시 유저의 보유 코인 감소
    public void decreaseCoin(float coin) {
        userCoin -= coin;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public float getUserCoin() {
        return userCoin;
    }

    public void setUserCoin(float userCoin) {
        this.userCoin = userCoin;
    }


    public String toString() {
        String str = "";
        str += String.format("================Wallet ===========\n");
        str += String.format("User Name : %s\n", userName);
        str += String.format("User Coin : %.2f\n", userCoin);
        str += String.format("==================================\n");
        return str;
    }

}