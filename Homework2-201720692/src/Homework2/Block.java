package Homework2;
import java.util.ArrayList;

public class Block {
    private String blockID;
    private String blockMaker;
    private int blockHeight;
    private String timestamp;
    private String nonce;
    private String preBlockID;
    private String merkleRoot;
    private MerkleTree merkleTree;
    private ArrayList<Transaction> confirmingTransaction;
    public static final int transactionNum = 4;

    public Block() {}
    public Block(String blockID,
                 String blockMaker, int blockHeight,
                 String timestamp,
                 String Nonce,
                 String preBlockID,
                 String merkleRoot,
                 MerkleTree merkleTree,
                 ArrayList<Transaction> transactions) {
        this.confirmingTransaction = new ArrayList<Transaction>();
        this.blockID = blockID;
        this.blockMaker = blockMaker;
        this.blockHeight = blockHeight;
        this.timestamp = timestamp;
        this.nonce = Nonce;

        if (blockHeight == 0) {
            this.preBlockID = "";
        }
        else {
            this.preBlockID = preBlockID;
        }

        this.merkleTree = merkleTree;
        this.merkleRoot = merkleRoot;
        this.confirmingTransaction = transactions;

    }


    public static boolean checkHashcash(String hash) {
        return hash.startsWith("000");
    }

    public String getHash(String preBlockId) {
        String text = preBlockId + merkleRoot + nonce;
        String hash = Utils.getSHA256(text);
        return hash;
    }

    public static String getValidHash(String preBlockId, String merkleRoot, String nonce) {
        String text = preBlockId + merkleRoot + nonce;
        String hash = Utils.getSHA256(text);
        if(checkHashcash(hash)) {
            return hash;
        }
        else {
            return "";
        }
    }

    public boolean isValid() {
        if(getValidHash(preBlockID, merkleRoot, nonce).isEmpty()) {
            return false;
        }
        return true;
    }



    public String getBlockID() {
        return blockID;
    }

    public void setBlockID(String blockID) {
        this.blockID = blockID;
    }

    public String getBlockMaker() {
        return blockMaker;
    }

    public void setBlockMaker(String blockMaker) {
        this.blockMaker = blockMaker;
    }

    public long getBlockHeight() {
        return blockHeight;
    }

    public void setBlockHeight(int blockHeight) {
        this.blockHeight = blockHeight;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getPreBlockID() {
        return preBlockID;
    }

    public void setPreBlockID(String preBlockID) {
        this.preBlockID = preBlockID;
    }

    public String getMerkleRoot() {
        return merkleRoot;
    }

    public void setMerkleRoot(String merkleRoot) {
        this.merkleRoot = merkleRoot;
    }

    public MerkleTree getMerkleTree() {
        return merkleTree;
    }

    public void setMerkleTree(MerkleTree merkleTree) {
        this.merkleTree = merkleTree;
    }

    public ArrayList<Transaction> getConfirmingTransaction() {
        return confirmingTransaction;
    }

    public void setConfirmingTransaction(ArrayList<Transaction> confirmingTransaction) {
        this.confirmingTransaction = confirmingTransaction;
    }

    @Override
    public String toString() {
        String str = "";
        str += String.format("=========================Block Head=================\n");
        str += String.format("Block ID %s\n", blockID);
        str += String.format("Block Maker %s\n", blockMaker);
        str += String.format("Block Height %d\n", blockHeight);
        str += String.format("Time Stamp : %s\n", timestamp);
        str += String.format("PreBlock ID : %s\n", preBlockID);
        str += String.format("Merkle Root : %s\n", merkleRoot);
        str += String.format("Nonce : %s\n", nonce);
        str += String.format("===========================Block Body===============\n");
        str += confirmingTransaction.toString();
        return str;
    }
}