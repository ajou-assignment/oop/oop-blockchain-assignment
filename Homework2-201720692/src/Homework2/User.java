package Homework2;

import javax.xml.bind.ValidationException;
import java.util.ArrayList;
import java.util.List;

public class User {
    protected Ledger publicLedger = new Ledger();
    protected List<Wallet> wallets = new ArrayList<Wallet>();

    public User(Ledger ledger, ArrayList<Wallet> wallet) {
        //connectedUser = new ArrayList<User>();
        this.publicLedger = ledger;
        this.wallets = wallet;
    }

    //Transactions 생성한다.
    public void sendTransaction(Wallet sender, Wallet receiver, float coin, float fee) {
        Transaction transaction = new Transaction(sender, receiver, coin, fee);
        publicLedger.addUnconfirmedTransaction(transaction);
    }


    public Transaction createRandomTransaction(boolean isSend) {
        Wallet sender = wallets.get(Utils.Rand.Int(wallets.size()));
        Wallet receiver = wallets.get(Utils.Rand.Int(wallets.size()));
        float coin = sender.getRandomCoinToSend();
        float fee = coin * 0.1f;
        if(isSend) {
            sendTransaction(sender, receiver, coin, fee);
        }
        Transaction transaction = new Transaction(sender, receiver, coin, fee);
        return transaction;
    }

    public void checkNewBlock() throws ValidationException {
        if (publicLedger.isNewBlockCreated()) {
            validateNewBlock();
            updateWallet();
            publicLedger.cleanNewBlock();
        }
    }

    public void validateNewBlock() throws ValidationException {
        if(publicLedger.isNewBlockCreated()) {
            Block block = publicLedger.getNewBlock();

            Block preBlock = publicLedger.getRecentBlock();
            String preBlockId = "";
            if(preBlock != null) {
                preBlockId = preBlock.getBlockID();
            }
            String validBlockHash = block.getHash(preBlockId);
            if (block.getBlockID().equals(validBlockHash) == false) {
                publicLedger.cleanNewBlock();
                throw new ValidationException("블록 검증 실패!");
            }

            publicLedger.addBlock(block);
            ArrayList<Transaction> confirmingTransaction = block.getConfirmingTransaction();
            for (Transaction transaction : confirmingTransaction) {
                publicLedger.getUnconfirmedTransaction().remove(transaction);
            }
        }
    }

    public void updateWallet() {
        if(publicLedger.isNewBlockCreated()) {
            Block block = publicLedger.getNewBlock();
            ArrayList<Transaction> confirmingTransaction = block.getConfirmingTransaction();
            for (Transaction transaction : confirmingTransaction) {
                Wallet senderWallet = transaction.getSenderWallet();
                float coin = transaction.getCoin();
                float fee = transaction.getFee();
                if (senderWallet != null) {
                    float senderDecreasedCoin = coin + fee;
                    senderWallet.decreaseCoin(senderDecreasedCoin);
                }
                float receiverIncreasedCoin = coin;
                transaction.getReceiverWallet().increaseCoin(receiverIncreasedCoin);

                Wallet minerWallet = findWallet(block.getBlockMaker());
                minerWallet.increaseCoin(fee);
            }
        }
    }

    protected Wallet findWallet(String username) {
        for(Wallet wallet : wallets) {
            if(wallet.getUserName().equals(username)) {
                return wallet;
            }
        }
        throw new IllegalArgumentException("존재하지 않는 유저 입니다!");
    }
}
