package Homework2;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.Date;
import java.util.Random;


public class Utils {
    public static String getSHA256(String str) {
        String SHA = "";
        try {
            MessageDigest sh = MessageDigest.getInstance("SHA-256");
            sh.update(str.getBytes());
            byte byteData[] = sh.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            SHA = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            SHA = null;
        }
        return SHA;
    }

    public static boolean isFloat(String value) {
        //TODO : .0 1. true ?
        try{
            Float.parseFloat(value);
            return true;
        }catch(NumberFormatException e){
            throw new IllegalArgumentException("매개변수는 Float 타입이여야 합니다.");
        }
    }

    public static boolean isInt(String value) {
        try{
            Integer.parseInt(value);
            return true;
        }catch(NumberFormatException e){
            throw new IllegalArgumentException("매개변수는 Int 타입이여야 합니다.");
        }
    }

    public static String getTimestamp() {
        //2018-05-20 00:38:53.187
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
        return sdf.format(new Date());
    }

    public static class Rand {
        public static int Int() {
            Random random = new Random();
            return random.nextInt();
        }

        public static int Int(int bound) {
            Random random = new Random();
            return random.nextInt(bound);
        }

        public static float Float() {
            Random random = new Random();
            return random.nextFloat();
        }
    }

}