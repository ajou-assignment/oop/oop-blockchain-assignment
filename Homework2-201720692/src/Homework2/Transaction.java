package Homework2;


public class Transaction {

    private String txID;
    private String sender;
    private float coin;
    private String receiver;
    private float fee;
    private String timeStamp;

    private Wallet senderWallet;
    private Wallet receiverWallet;


    public Transaction(Wallet sender, Wallet receiver, float coin, float fee) {
        this.sender = sender.getUserName();
        this.receiver = receiver.getUserName();
        this.coin = coin;
        this.fee = fee;
        this.timeStamp = Utils.getTimestamp();

        senderWallet = sender;
        receiverWallet = receiver;
        setTxId();
    }

    public Transaction(Wallet receiver, float coin, float fee) {
        this.receiver = receiver.getUserName();
        this.coin = coin;
        this.fee = fee;
        this.timeStamp = Utils.getTimestamp();

        senderWallet = null;
        receiverWallet = receiver;
        setTxId();
    }


    private void setTxId() {
        String text = "";
        text += sender;
        text += receiver;
        text += coin;
        text += fee;
        text += timeStamp;
        String hash = Utils.getSHA256(text);
        this.txID = hash;
    }


    public String getTxID() {
        return txID;

    }

    public void setTxID(String txID) {
        this.txID = txID;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public float getCoin() {
        return coin;
    }

    public void setCoin(float coin) {
        this.coin = coin;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public float getFee() {
        return fee;
    }

    public void setFee(float fee) {
        this.fee = fee;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }



    public Wallet getSenderWallet() {
        return senderWallet;
    }

    public Wallet getReceiverWallet() {
        return receiverWallet;
    }


    @Override
    public String toString()
    {
        String str = "";
        str += String.format("=======================Transaction %s===============\n", txID);
        String sender = this.sender;
        if(sender == null) {
            sender = "";
        }
        str += String.format("Sender : %s\n", sender);
        str += String.format("Receiver : %s\n", receiver);
        str += String.format("Coin : %f\n", coin);
        str += String.format("Fee : %f\n", fee);
        str += String.format("TimeStamp : %s\n", timeStamp);
        str += String.format("===============================================\n");
        return str;
    }

}